(function ($, Drupal) {
  Drupal.behaviors.astonish = {
    attach: function (context) {
      console.log(this);
      this.preLoader(context);
      this.sticyNav(context);
    },
    preLoader: function(context){
      $("#preloader", context).animate({
        'opacity': '0'
      }, 600, function(){
        setTimeout(function(){
          $("#preloader", context).css("visibility", "hidden").fadeOut();
        }, 300);
      });
    },
    sticyNav: function(content){
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var box = $('.header-text').height();
        var header = $('header').height();
  
        if (scroll >= box - header) {
        $("header").addClass("background-header");
        } else {
        $("header").removeClass("background-header");
        }
      });
    }
  }
})(jQuery, Drupal);